"""Author: Mitchell Bratina

"""

import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model, preprocessing
from sklearn.impute import SimpleImputer as SImp
from format import aggregate_json_to_dataframe, get_clean_csi_data, reduce_dimensionality, only_open_closed

def main(version : str = 'new', path : str ='none', param_finder : bool =False):
    if (path == 'none'):
        path = input("Enter path to data: ")
    
    tempdata = aggregate_json_to_dataframe(path)

    data = get_clean_csi_data(tempdata, version=version)
    # data = reduce_dimensionality(data)

    X = data
    Y = tempdata.get("testang")
    Y = only_open_closed(Y)

    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)
    
    n_estimators = 1000
    min_samples_split = 2
    min_samples_leaf = 1



    if (param_finder == False):
        model = RandomForestClassifier(n_jobs=-1)

        model.fit(x_train, y_train)
        acc = model.score(x_test, y_test)
        print(acc)
    else:
        while min_samples_split <= 10:
            model = RandomForestClassifier(n_jobs=-1, n_estimators=n_estimators,
                                        min_samples_split=min_samples_split,
                                        min_samples_leaf=min_samples_leaf,
                                        criterion='entropy')

            model.fit(x_train, y_train)
            acc = model.score(x_test, y_test)
        
            print(f"n_estimators: {n_estimators}")
            print(f"min_samples_split: {min_samples_split}")
            print(f"min_samples_leaf: {min_samples_leaf}")
            print(f"ACCURACY: {acc}")
            print('-------------------------')
            print()

            n_estimators += 1
            if (n_estimators > 1000):
                n_estimators = 10
                min_samples_leaf += 1
            if (min_samples_leaf > 10):
                min_samples_leaf = 1
                min_samples_split += 1



if __name__ == "__main__":
    main()