"""Author: Mitchell Bratina

"""

import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model, preprocessing
from sklearn.impute import SimpleImputer as SImp
from format import aggregate_json_to_dataframe, get_clean_csi_data

tempdata = aggregate_json_to_dataframe("./door_data_small")

data = get_clean_csi_data(tempdata, version='new')

X = data
Y = tempdata.get("testang")


print(Y.size)
print(Y)

for index in range(Y.size):
    if Y[index] == 0:
        Y[index] = 1

print(Y)
print()
print(Y[2])
