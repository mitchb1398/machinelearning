""" Author: Mitchell Bratina

"""

import pandas as pd
import os
from sklearn.impute import SimpleImputer as SImp
from sklearn.feature_selection import VarianceThreshold as VarTh

def aggregate_json_to_dataframe(dataFolderPath : str, debug : bool =False):
    """Pulls json files from a specified folder and
        combines them into one dataframe

    Parameters
    ----------
    dataFolderPath : str
        The path to the folder contains json files

    debug : bool
        Turn debugging off and on

    Return
    ------
    cumulativeFrame : JsonReader
        The combination of all the json files in a dataframe-like structure
    """
    doorDataFolder = dataFolderPath
    count = 0
    for file in os.scandir(doorDataFolder):

        if debug:
            print(count)

        if (file.path.endswith(".json") and (os.path.getsize(file.path) != 0)):
            if (count == 0):
                cumulativeFrame = pd.read_json(file)
                count += 1
            else:
                tempFrame = pd.read_json(file)
                cumulativeFrame = cumulativeFrame.append(tempFrame, ignore_index=True)
                count += 1
    if debug:
        print(cumulativeFrame)
        print(count)
    
    return cumulativeFrame

def get_clean_csi_data(dataFrame : pd.DataFrame, replaceNumber : int =0, version : str ='new'):
    """Cleans and returns a dataframe
    Converts strings in data to numeric
    Replaces missing numbers

    Parameters
    ----------
    dataFrame : pd.DataFrame
        The complete json dataframe with headers and data

    replaceNumber : int
        Number to replace any missing numbers in the CSI data

    Return
    ------
    data : pd.DataFrame
        Dataframe with filled missing values
    """

    tempdata = pd.DataFrame(dataFrame.get("data"))
    data = pd.DataFrame(tempdata.data.tolist(), index=tempdata.index)

    if (version == 'old'):
        for col in range(0, 384):
            data[col] = pd.to_numeric(data[col])

    imp = SImp(strategy='constant', fill_value=0)
    imp.fit(data)
    data = imp.transform(data)

    return data

def reduce_dimensionality(dataFrame : pd.DataFrame, debug : bool =False):
    """Reduces the dimensionality of the data
    which was originally 384 dimensions.

    Parameters
    ----------
    dataFrame : pd.DataFrame
        The dataframe with only the CSI signal data

    Return
    ------
    reducedDataFrame : pd.DataFrame
        The dataframe with the signal data, but with
        reduced dimensionality
    """

    reducer = VarTh(threshold=125)
    reducedDataFrame = reducer.fit_transform(dataFrame)

    if debug:
        print("Original:")
        print(dataFrame.shape)
        print("New:")
        print(reducedDataFrame.shape)
    
    return reducedDataFrame

def only_open_closed(angleSeries):
    """Changes any angles that are not zero
    to one so that the program only tries
    to predict open and close.

    Parameters
    ----------
    angleSeries
        A dataseries from pandas of the angles

    Return
    ------
    angleSeries
        This is messy, but we only make the changes
        and return the same data series
    """

    for index in range(angleSeries.size):
        if angleSeries[index] != 0:
            angleSeries[index] = 1

    return angleSeries