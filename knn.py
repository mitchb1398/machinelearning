"""Author: Mitchell Bratina

"""

import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model, preprocessing
from sklearn.impute import SimpleImputer as SImp
from format import aggregate_json_to_dataframe, get_clean_csi_data, reduce_dimensionality, only_open_closed
from sklearn.metrics import average_precision_score

def main(version : str = 'new', path : str ='none'):
    if (path == 'none'):
        path = input("Enter path to data: ")
    
    tempdata = aggregate_json_to_dataframe(path)

    data = get_clean_csi_data(tempdata, version=version)
    # data = reduce_dimensionality(data)

    X = data
    Y = tempdata.get("testang")
    Y = only_open_closed(Y)

    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)

    neighbors = 1
    acc = 0.0
    while (acc < 0.9 and neighbors <= 15):
    
        model = KNeighborsClassifier(n_neighbors=neighbors, n_jobs=-1)

        model.fit(x_train, y_train)
        acc = model.score(x_test, y_test)
        print(f"Neighbors: {neighbors}    Accuracy: {acc}")
        neighbors += 1

if __name__ == "__main__":
    main()
