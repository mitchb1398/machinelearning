"""
Author: Mitchell Bratina
"""

import pickle
from numpy.core.fromnumeric import std
import pandas as pd
import numpy as np
import sklearn
from sys import stdin, stdout
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model, preprocessing
from sklearn.impute import SimpleImputer as SImp
from format import aggregate_json_to_dataframe, get_clean_csi_data, reduce_dimensionality, only_open_closed

def main(version : str ='new', path : str ='none', trainNew : bool =False, maxTrains : int =15):
    """Runs either classification of a data point with
    current model or trains a new model.

    Parameters
    ----------
    version : str
        Determines the version of method to get
        clean csi data. Will almost always certainly
        want this set to 'new'.

    path : str
        Path to the folder containing csi jsons.

    trainNew : bool
        Determines whether the program will train a new
        model. Leave on False unless you want the old
        model overwritten.

    maxTrains : int
        If program is training a new model, the number
        of models to make a test. Always saves the highest
        scoring model of all the models trained and tested.

    Returns
    -------
    If training: nothing;
    If predicting: prediction

    Other Info
    ----------
    Data form for prediction should be in the same form as
    data was for training. You can put any number in for
    the testang; it shouldn't matter.

    """

    if (trainNew == True):
        # This section runs if training a new algorithm
        if (path == 'none'):
            path = input("Enter path to data: ")
        
        tempdata = aggregate_json_to_dataframe(path)

        data = get_clean_csi_data(tempdata, version=version)
        # Reduce dimensionality is turned off because it didn't seem to help
        # data = reduce_dimensionality(data)

        X = data
        Y = tempdata.get("testang")
        Y = only_open_closed(Y)

        print("Will only print accuracy of runs that improve upon previous accuracy.")
        lastHighestAcc = 0.0
        run = 0
        while (run < maxTrains):
            x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)
            model = RandomForestClassifier(n_jobs=-1)

            model.fit(x_train, y_train)
            acc = model.score(x_test, y_test)

            if (acc > lastHighestAcc):
                print(acc)
                lastHighestAcc = acc
                with open("./RForestAlgo.mitch", 'wb') as outFile:
                    pickle.dump(model, outFile)
            
            run += 1
        print("Done training")
    else:
        # This section runs if predicting (not training a new algo)
        with open("./RForestAlgo.mitch", 'rb') as inFile:
            model = pickle.load(inFile)

        while (True):
            for line in stdin:
                tempdata = pd.read_json(line)
                data = get_clean_csi_data(tempdata, version=version)

                predictionList = model.predict(data)
                probabilityList = model.predict_proba(data)

                # Right now, just printing the prediction, can do whatever you like
                # print(f"Prediction: {prediction}, Probability: {probability}")

                for index in range(len(predictionList)):
                    prediction = predictionList[index]
                    probability = probabilityList[index][prediction]
                    print(f"Prediction: {prediction}, Probability: {probability}")
                
if __name__ == "__main__":
    main()