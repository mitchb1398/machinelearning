"""Author: Mitchell Bratina

"""

from os import wait, wait3
from numpy.core.fromnumeric import mean, var
from numpy.lib.function_base import average
import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
from sklearn import linear_model, preprocessing
from sklearn.impute import SimpleImputer as SImp
from sklearn.metrics import accuracy_score
from format import aggregate_json_to_dataframe, get_clean_csi_data, reduce_dimensionality, only_open_closed
import math
import os

def main(version : str = 'new', path : str ='none'):
    if (path == 'none'):
        path = input("Enter path to data: ")
    
    tempdata = aggregate_json_to_dataframe(path)

    data = get_clean_csi_data(tempdata, version=version)
    # data = reduce_dimensionality(data)

    X = data
    Y = tempdata.get("testang")
    Y = only_open_closed(Y)

    runs = 10
    accList = []
    recList = []
    precList = []
    for _ in range(runs):
        x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, Y, test_size=0.1)
        
        model = RandomForestClassifier(n_jobs=-1)

        model.fit(x_train, y_train)

        predicted = model.predict(x_test)
        print(predicted)

        acc = accuracy_score(predicted, y_test)
        recAndPrec = get_rec_and_prec(predicted, y_test.to_numpy())
        rec = recAndPrec[0]
        prec = recAndPrec[1]
        
        accList.append(acc)
        recList.append(rec)
        precList.append(prec)

        print(f"Acc: {acc}, Rec: {rec}, Prec: {prec}")
        print(model.get_params())

    meanAcc = mean(accList)
    variance = var(accList)

    meanRec = mean(recList)
    meanPrec = mean(precList)
    
    print(f"Average accuracy for {runs} runs: {meanAcc}")
    print(f"Variance for {runs} runs: {variance}")
    print(f"Maximum accuracy for {runs} runs: {max(accList)}")
    print(f"Minimum accuracy for {runs} runs: {min(accList)}")
    print(f"Average recall for {runs} runs: {meanRec}")
    print(f"Average precision for {runs} runs: {meanPrec}")

    while (True):
        os.system('say "Mitchell, your run is finished."')
        

def get_rec_and_prec(predicted, actual):
    truePositive = 0.0
    falseNegative = 0.0
    falsePositive = 0.0

    for i in range(len(actual)):
        if actual[i] == predicted[i] == 1:
            truePositive += 1.0
        if (actual[i] == 1) and (predicted[i] == 0):
            falseNegative += 1.0
        if (actual[i] == 0) and (predicted[i] == 1):
            falsePositive += 1.0

    recall = truePositive/(truePositive + falseNegative)
    precision = truePositive/(truePositive + falsePositive)
    recAndPrec = [recall, precision]
    return recAndPrec


if __name__ == "__main__":
    main()