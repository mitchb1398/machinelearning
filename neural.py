import tensorflow as tf
from tensorflow import keras
import pandas as pd
import numpy as np
import pickle
from format import aggregate_json_to_dataframe, get_clean_csi_data, reduce_dimensionality, only_open_closed

def main(version : str = 'new', path : str ='none'):
    if (path == 'none'):
        path = input("Enter path to data: ")
    
    tempdata = aggregate_json_to_dataframe(path)

    data = get_clean_csi_data(tempdata, version=version)
    data = reduce_dimensionality(data)

    X = data
    Y = tempdata.get("testang")
    Y = only_open_closed(Y)

    dataset = tf.data.Dataset.from_tensor_slices((X, Y))
    train_dataset = dataset.shuffle(len(X)).batch(1)

    model = get_model()
    # model = pickle.load('./NeuralNet.mitch')
    model.fit(train_dataset, epochs=5)
    pickle.dump(model, './NeuralNet.mitch')


def get_model():
    model = tf.keras.Sequential([
        tf.keras.layers.Dense(10, activation='relu'),
        tf.keras.layers.Dense(10, activation='elu'),
        tf.keras.layers.Dense(1)
    ])

    model.compile(optimizer='adam',
                    loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                    metrics=['accuracy'])
    return model

if __name__ == "__main__":
    main()